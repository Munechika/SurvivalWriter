﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SurvivalWriter
{
    public partial class Standard : Form
    {
        public Standard()
        {
            InitializeComponent();
        }

        private void Standard_Load(object sender, EventArgs e)
        {
            richTextBox1.Paste();

            int textLength = richTextBox1.TextLength;
            toolStripStatusLabel2.Text = textLength.ToString();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DialogResult retry;
            retry = MessageBox.Show("サバイバルへ再突入します。今書いている文章は削除されますが本当にいいんですか？", "再突入", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (retry == DialogResult.OK)
            {
                Form1 surv = new Form1();
                surv.Show();
                Hide();
            }
        }

        private void Standard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DialogResult tfont;
            tfont = fontDialog1.ShowDialog();
            if (tfont == DialogResult.OK)
            {
                richTextBox1.SelectionFont = fontDialog1.Font;
                richTextBox1.SelectionColor = fontDialog1.Color;
            }
        }

        private void 上書き保存SToolStripButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog SDCard = new SaveFileDialog();
            String savefilename = SDCard.FileName;
            SDCard.Filter = "リッチテキスト|*.rtf|テキストファイル|*.txt";

            if (SDCard.ShowDialog() == DialogResult.OK && SDCard.FileName.Length > 0)
            {
                switch (SDCard.FilterIndex)
                {
                    case 1:
                        richTextBox1.SaveFile(SDCard.FileName, RichTextBoxStreamType.RichText);
                        MessageBox.Show("保存しました", "完了", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;

                    case 2:
                        richTextBox1.SaveFile(SDCard.FileName, RichTextBoxStreamType.PlainText);
                        MessageBox.Show("保存しました", "完了", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                }
            }
        }

        private void ヘルプLToolStripButton_Click(object sender, EventArgs e)
        {
            About help = new About();
            help.ShowDialog();
        }
    }
}
